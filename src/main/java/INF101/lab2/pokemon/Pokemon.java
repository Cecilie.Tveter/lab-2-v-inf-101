package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {
    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random random;


    public Pokemon (String name) {
        this.name = name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
    }

    public String getName() {
        return name;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getCurrentHP() {
        return healthPoints;
    }

    @Override
    public int getMaxHP() {
        return maxHealthPoints;
    }

    public boolean isAlive() {
        if (getCurrentHP() > 0) {
            return true;

        }
        return false;

    }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        target.damage(damageInflicted);
        if (!target.isAlive()) {
            System.out.println(target + " is defeated by " + getName());
        }
        else {
            damage(damageInflicted);
            System.out.println(getName() + "attacks" + target);
        }
    }

    @Override
    public void damage(int damageTaken) {
        if (healthPoints > 0 && damageTaken > 0) {
            this.healthPoints = healthPoints - damageTaken;
            if (damageTaken < 0 || healthPoints < 0) {
                damageTaken = 0;
                this.healthPoints = 0;
                System.out.println(getName() + " takes " + damageTaken + " and is left with " + this.healthPoints + " / " + maxHealthPoints);
            }
        }
    }


    @Override
    public String toString() {
        String string = this.getName() + " HP: " + "(" + getCurrentHP() + "/"
                + getMaxHP() + ") " + "STR:  " + getStrength();
        return string;
    }

}
